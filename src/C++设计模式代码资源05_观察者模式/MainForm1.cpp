class MainForm : public Form
{
	TextBox *txtFilePath;
	TextBox *txtFileNumber;
	// 修改
	ProgressBar *progressBar;

public:
	void Button1_Click()
	{

		string filePath = txtFilePath->getText();
		int number = atoi(txtFileNumber->getText().c_str());

		// 修改
		FileSplitter splitter(filePath, number, progressBar);

		splitter.split();
	}
};
